package com.pwc.addressbook.Impl;

import com.pwc.addressbook.dao.AddressRepo;
import com.pwc.addressbook.model.Address;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class AddressBookServiceImplTest {

    @Mock
    private AddressBookServiceImpl underTest;

    @Mock private AddressRepo addressRepo;

    @BeforeEach
    void setUp() {
        underTest = Mockito.spy(new AddressBookServiceImpl(addressRepo));
    }

    @AfterEach
    void tearDown() {

    }

    @Test
    void getAllAddress() {
        underTest.getAllAddress();

        verify(addressRepo).findAll();
    }

    @Test
    void saveAddress() {
        Address address = new Address(1,"Betty", 909989090L,"user1");

        underTest.saveAddress(address);

        ArgumentCaptor<Address> addressArgumentCaptor = ArgumentCaptor.forClass(Address.class);

        verify(addressRepo).save(addressArgumentCaptor.capture());

        Address capturedAddress= addressArgumentCaptor.getValue();

        assertThat(capturedAddress).isEqualTo(address);

    }

    @Test
    void getUniqueAddress() {
        Address stubAdd1 = new Address(1,"Betty", 909099L,"user1");
        List<Address> user1AddressList = Arrays.asList(stubAdd1);
        Address stubAdd2 = new Address(2,"John", 909099L,"user2");
        List<Address> user2AddressList = Arrays.asList(stubAdd2);

        doReturn(user1AddressList)
                .when(underTest).getAllAddressByUser("user1");
        doReturn(user2AddressList)
                .when(underTest).getAllAddressByUser("user2");

        assertEquals(2,underTest.getUniqueAddress("user1","user2").size());
    }

    @Test
    void getAllAddressByUser() {
        String user = "user1";
        underTest.getAllAddressByUser(user);

        verify(addressRepo).findByUserName(user, Sort.by(Sort.Direction.ASC, "name"));
    }
}