package com.pwc.addressbook.controller;

import com.pwc.addressbook.Service.AddressBookService;
import com.pwc.addressbook.model.Address;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest
class AddressBookControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AddressBookService addressBookService;

    @Test
    @Disabled
    void saveAddress() {
    }

    @Test
    void testGetAll() throws Exception {
        Address address1 = new Address(1,"Betty", 909090909L,"user1");
        Address address2 = new Address(2,"Jane", 909090909L,"user1");

        Mockito.when(addressBookService.getAllAddress()).thenReturn(Arrays.asList(address1,address2));

        mockMvc.perform(MockMvcRequestBuilders.get("/getAllAddress")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void getUniqueAddress() throws Exception {
        String uniqueFriend = "Betty";

        Mockito.when(addressBookService.getUniqueAddress("user1","user2")).thenReturn(Arrays.asList(uniqueFriend));

        mockMvc.perform(MockMvcRequestBuilders.get("/getUniqueAddress").param("firstUser","user2")
                .param("secondUser","user2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}