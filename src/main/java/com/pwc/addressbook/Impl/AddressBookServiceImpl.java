package com.pwc.addressbook.Impl;

import com.pwc.addressbook.Service.AddressBookService;
import com.pwc.addressbook.dao.AddressRepo;
import com.pwc.addressbook.model.Address;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@AllArgsConstructor
public class AddressBookServiceImpl implements AddressBookService {
    @Autowired
    AddressRepo addressRepo;

    @Override
    public List<Address> getAllAddress() {
        return addressRepo.findAll();
    }

    @Override
    public void saveAddress(Address address) {
        addressRepo.save(address);
    }

    @Override
    public List<String> getUniqueAddress(String firstUser, String secondUser) {
        List<Address> firstUserAddressBook = getAllAddressByUser(firstUser);
        List<Address> secondUserAddressBook = getAllAddressByUser(secondUser);

        List<String> book1 = firstUserAddressBook.stream().map(i -> i.getName()).collect(Collectors.toList());
        List<String> book2 = secondUserAddressBook.stream().map(i -> i.getName()).collect(Collectors.toList());

        return findUniqueElements(book1, book2);
    }

    @Override
    public List<Address> getAllAddressByUser(String user) {
        return addressRepo.findByUserName(user, Sort.by(Sort.Direction.ASC,"name"));
    }

    private List<String> findUniqueElements(List<String> book1, List<String> book2) {
        HashMap<String,Integer> namesWithFrequency = new HashMap<>();
        saveNamesWithFrequency(book1, namesWithFrequency);
        saveNamesWithFrequency(book2, namesWithFrequency);
        return  namesWithFrequency.entrySet().stream().filter(j->j.getValue()==1).
                map(j->j.getKey()).flatMap(Stream::of).
                collect(Collectors.toList());
    }

    private void saveNamesWithFrequency(List<String> book1, HashMap<String, Integer> namesWithFrequency) {
        for(String name: book1){
            if(namesWithFrequency.get(name) == null) {
                namesWithFrequency.put(name,1);
            } else {
                namesWithFrequency.put(name, namesWithFrequency.get(name)+1);
            }
        }
    }
}
