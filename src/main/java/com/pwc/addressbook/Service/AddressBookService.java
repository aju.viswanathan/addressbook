package com.pwc.addressbook.Service;

import com.pwc.addressbook.model.Address;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AddressBookService {

    public List<Address> getAllAddress();

    public void saveAddress(Address address);

    public List<String> getUniqueAddress(String firstUser, String secondUser);

    public List<Address> getAllAddressByUser(String user);
}
