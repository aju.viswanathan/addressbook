package com.pwc.addressbook.dao;

import com.pwc.addressbook.model.Address;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddressRepo extends JpaRepository<Address ,Integer> {

    List<Address> findByUserName(String user, Sort name);
}
