package com.pwc.addressbook.controller;

import com.pwc.addressbook.Service.AddressBookService;
import com.pwc.addressbook.model.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AddressBookController {

    @Autowired
    AddressBookService addressBookService;

    @PostMapping("/saveAddress")
    public String saveAddress(@RequestBody Address address) {
        addressBookService.saveAddress(address);
        return "save successful";
    }

    @GetMapping("/getAllAddress")
    public ResponseEntity<List<Address>> getAll(){
        return new ResponseEntity<>(addressBookService.getAllAddress(), HttpStatus.OK);
    }

    @GetMapping("/getAllAddressByUser/{user}")
    public List<Address> getAllByUser(@PathVariable String user){
        return addressBookService.getAllAddressByUser(user);
    }

    @GetMapping("/getUniqueAddress")
    public List<String> getUniqueAddress(@RequestParam String firstUser, @RequestParam String secondUser) {
        return addressBookService.getUniqueAddress(firstUser,secondUser);
    }

}
