# AddressBook

Address Book Implementation(Spring boot)

## Postman request for APIs

GetAllAddress  http://localhost:8080/getAllAddress
    Returns the List all the data in address table

GetUniqueAddress  http://localhost:8080/getUniqueAddress?firstUser=user1&secondUser=user2
    Returns List of string of contact names unique to two users

GetAllAddressByUser  http://localhost:8080/getAllAddressByUser/user1
    Returns the list of contacts present for a given user sorted by the names

